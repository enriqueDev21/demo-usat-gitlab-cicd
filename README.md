# **Demo USAT GitLab CI/CD**

Para esta demo se construirá una pipeline para desplegar una página web sencilla en un máquina virtual remota.

![terraform](./docs/devops-gitlab.png)

## **Recursos**

* https://about.gitlab.com/topics/devops/
* https://docs.gitlab.com/ee/ci/
